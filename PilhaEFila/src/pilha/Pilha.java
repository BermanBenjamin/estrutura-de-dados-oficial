package pilha;

public class Pilha implements IPilha {
	No fim;

	public void push(int valor) {
		No novo = new No();
		novo.valor = valor;
		novo.embaixo = fim;
		fim = novo;

	}

	public No pop() {
		No aux = fim;
		fim = fim.embaixo;
		return aux;
	}

	public void imprimir() {
		No aux = fim;
		while(aux != null) {
			System.out.println(aux.valor);
			aux = aux.embaixo;
		}
	}
}

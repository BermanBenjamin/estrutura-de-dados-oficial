package questao03;

import fila.Fila;

public class FilaMista {
	Fila ordenar(Fila normal, Fila preferencial) {
		Integer contador = 0;
		Fila mista = new Fila();
		while(contador < normal.tamanho() + preferencial.tamanho()) {
			if(contador%3 == 0) {
				mista.inserir(preferencial.remover().valor);
			}else {
				mista.inserir(preferencial.remover().valor);
			}
			contador++;
		}
		
		return mista;
		
	}
}

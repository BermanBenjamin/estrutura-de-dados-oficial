package questao01;

public class Fila implements IFila {
	No primeiro = null;

	@Override
	public void inserir(int valor) {
		No novo = new No();
		novo.valor = valor;
		if (primeiro == null) {
			primeiro = novo;
		} else {
			No aux = primeiro;
			while (aux.prox != null) {
				aux = aux.prox;
			}
			aux.prox = novo;
		}

	}

	@Override
	public No remover() {
		No aux = primeiro;
		if (primeiro != null) {
			primeiro = primeiro.prox;
		}
		return aux;
	}

	@Override
	public void imprimir() {
		No aux = primeiro;
		while (aux != null) {
			System.out.println(aux.valor);
			aux = aux.prox;
		}
	}

	@Override
	public Integer tamanho() {
		Integer tamanho = 0;
		No aux = primeiro;
		while (aux != null) {
			tamanho++;
			aux = aux.prox;
		}
		return tamanho;
	}

}
